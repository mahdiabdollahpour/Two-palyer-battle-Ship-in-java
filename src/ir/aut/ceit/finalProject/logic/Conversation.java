package ir.aut.ceit.finalProject.logic;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.view.ConversationHistory;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.event.MouseInputListener;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class Conversation {

    private Conversation mInstance = this;
    private String mContactName;
    private String mContactIP;

    public String getID() {
        return mID;
    }

    private String mID;
    private ArrayList<JSONObject> mChat = new ArrayList<>();

    public Conversation(String ID) {
        mID = ID;
        mChat = new ArrayList<>();
        mContactName = Main.getOpponentName();
        mContactIP = Main.getOpponentIP();
    }

    public JLabel makeLabel() {
        JLabel label = null;
        try {
            if (mChat.size() > 0) {
                String s = "<html><body>" + mContactName + "<br>" +
                        mChat.get(mChat.size() - 1).getString("message") + "<br>" +
                        mChat.get(mChat.size() - 1).getString("date") + "</body></html>";
                label = new JLabel(s);
            } else {
                String s = "<html><body>" + mContactName + "<br>" + "no talk" + "</body></html>";
                label = new JLabel(s);
            }
            Dimension d = new Dimension(400, 200);
            label.setMinimumSize(d);
            label.setMaximumSize(d);
            label.setPreferredSize(d);
            label.setBorder(new LineBorder(Color.black, 5));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        label.addMouseListener(new MouseInputListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                ConversationHistory.openConversation(mInstance);
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }

            @Override
            public void mouseDragged(MouseEvent e) {

            }

            @Override
            public void mouseMoved(MouseEvent e) {

            }
        });
        return label;
    }

    public void addMessageToList(JSONObject jsonObject) {
        mChat.add(jsonObject);
    }

    public JSONObject getToJson() {
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < mChat.size(); i++) {
                jsonArray.put(i, mChat.get(i));
            }
//            if (mContactName == null) {
//                mContactName = "unknown";
//            }
//            if (mContactIP == null) {
//                mContactIP = "unknown";
//            }
            jsonObject.put("name", mContactName);
            jsonObject.put("IP", mContactIP);
            jsonObject.put("id", mID);
            jsonObject.put("chat", jsonArray);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return jsonObject;
    }

    public static Conversation parseJson(JSONObject jsonObject) throws JSONException {
        String id = jsonObject.getString("id");
        Conversation conversation = new Conversation(id);
        conversation.setContactName(jsonObject.getString("name"));
        conversation.setContactIP(jsonObject.getString("IP"));
        JSONArray jsonArray = jsonObject.getJSONArray("chat");
        ArrayList<JSONObject> chat = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            chat.add(jsonArray.getJSONObject(i));
        }
        conversation.setChat(chat);
        return conversation;
    }

    public String getContactName() {
        return mContactName;
    }


    public void setContactName(String name) {
        mContactName = name;
    }


    public String getContactIP() {
        return mContactIP;
    }


    public void setContactIP(String IP) {
        mContactIP = IP;
    }

    public ArrayList<JSONObject> getChat() {
        return mChat;
    }

    public void setChat(ArrayList<JSONObject> chat) {
        mChat = chat;
    }

}
