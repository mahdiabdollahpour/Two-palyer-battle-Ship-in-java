package ir.aut.ceit.finalProject.logic;

/**
 * Created by ASUS on 08/07/2017.
 */
public interface ReadyAndUnreadyCallback {
    void sendReadyMessage();
    void sendUnreadyMessage();
}
