package ir.aut.ceit.finalProject.view;

class Ship {

    private String kind;
    private int length;
    private int x;
    private int y;
    private String direction;

    Ship(String kind, int length, String direction, int x, int y){
        this.kind = kind;
        this.direction = direction;
        this.length = length;
        this.x = x;
        this.y = y;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    String getKind() {
        return kind;
    }

    int getLength() {
        return length;
    }

    int getX() {
        return x;
    }

    int getY() {
        return y;
    }

    String getDirection() {
        return direction;
    }
}
