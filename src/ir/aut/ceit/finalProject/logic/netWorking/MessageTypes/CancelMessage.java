package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

/**
 * Created by ASUS on 09/07/2017.
 */
public class CancelMessage extends BaseMessage {


    public CancelMessage(byte[] data) {
        mSerialized = data;
        deserialize();
    }

    private String ip;

    public String getIp() {
        return ip;
    }

    public CancelMessage() {
        serialize();
    }

    @Override
    public void serialize() {
        ip = Main.setMyIP();
        byte[] ipBytes = ip.getBytes();
        int ipBytesLen = ipBytes.length;
        int messageLength = 4 + 1 + 1 + 4 + ipBytesLen;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.CANCEL_MESSAGE);
        byteBuffer.putInt(ipBytesLen);
        byteBuffer.put(ipBytes);
        mSerialized = byteBuffer.array();
    }

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int ipbyteslen = byteBuffer.getInt();
        byte[] ipBytes = new byte[ipbyteslen];
        byteBuffer.get(ipBytes);
        ip = new String(ipBytes);
    }

    @Override
    public byte getMessageType() {
        return MessageTypes.CANCEL_MESSAGE;
    }

}
