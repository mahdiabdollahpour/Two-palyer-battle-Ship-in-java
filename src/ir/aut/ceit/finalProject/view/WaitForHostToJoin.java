package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.logic.CancelCallBack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class WaitForHostToJoin extends JFrame {
    private static WaitForHostToJoin instance;
    private JLabel mLabel;
    private JButton mCancel;
    public CancelCallBack mCancelAction;

    public static WaitForHostToJoin getInstance() {
        if (instance == null) {
            instance = new WaitForHostToJoin();
        }
        return instance;
    }

    private WaitForHostToJoin() {
        setLayout(new FlowLayout());
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(new Dimension(500, 200));
        mLabel = new JLabel();
        mCancel = new JButton("Cancel");
        add(mLabel);
        add(mCancel);
        getWaitingState();
        setTitle("Please Wait...");
    }

    public void getWaitingState() {
        mLabel.setText("Waiting for the host to join ...");
        mCancel.setVisible(true);

    }

    public void disPlayHostRejected(boolean youDidCancel) {
        if (youDidCancel) {
            mLabel.setText("Request Canceled");
        } else {
            mLabel.setText("Host rejected the connection");
        }
        mCancel.setVisible(false);
        repaint();
        revalidate();
    }

    public void setCancelAction(CancelCallBack cancelAction) {
        mCancelAction = cancelAction;
        mCancel.addActionListener(e -> mCancelAction.sendCancelMessage());
    }
}
