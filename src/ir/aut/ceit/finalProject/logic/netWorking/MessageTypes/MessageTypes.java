package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

public class MessageTypes {


    public static final byte PROTOCOL_VERSION = 1;
    public static final byte REQUEST_LOGIN = 1;
    public static final byte STRING_MESSAGE = 2;
    public static final byte ATTACK_MESSAGE = 3;
    public static final byte ATTACK_FEEDBACK_MESSAGE = 4;
    public static final byte LEAVE_MESSAGE = 5;
    public static final byte JOIN_MESSAGE = 6;
    public static final byte READY_MESSAGE = 7;
    public static final byte START_GAME_MESSAGE = 8;
    public static final byte UNREADY_MESSAGE = 9;
    public static final byte TURN_MESSAGE = 10;
    public static final byte REJECT_MESSAGE = 11;
    public static final byte VICTORY_MESSAGE = 12;
    public static final byte CANCEL_MESSAGE = 13;


}
