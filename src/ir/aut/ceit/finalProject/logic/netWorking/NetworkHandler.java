package ir.aut.ceit.finalProject.logic.netWorking;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.logic.netWorking.MessageTypes.*;

import javax.swing.*;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;

public class NetworkHandler extends Thread {
    private TcpChannel mTcpChannel;
    private Queue<byte[]> mSendQueue;
    private Queue<byte[]> mReceivedQueue;
    private ReceivedMessageConsumer mConsumerThread;
    private INetworkHandlerCallback mINetworkHandlerCallback;

    public NetworkHandler(SocketAddress socketAddress, INetworkHandlerCallback iNetworkHandlerCallback) {
        mSendQueue = new LinkedBlockingQueue<>();
        mReceivedQueue = new LinkedBlockingQueue<>();
        try {
            mTcpChannel = new TcpChannel(socketAddress, 1000);
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.mINetworkHandlerCallback = iNetworkHandlerCallback;
        mConsumerThread = new ReceivedMessageConsumer();
        mConsumerThread.start();
    }

    public NetworkHandler(Socket socket, INetworkHandlerCallback iNetworkHandlerCallback) {

        mSendQueue = new LinkedList<>();
        mReceivedQueue = new LinkedList<>();
        try {
            mTcpChannel = new TcpChannel(socket, 5000);
        } catch (IOException e) {
            e.printStackTrace();
        }
        this.mINetworkHandlerCallback = iNetworkHandlerCallback;
        mConsumerThread = new ReceivedMessageConsumer();
        mConsumerThread.start();

    }

    /**
     * Add serialized bytes of message to the sendQueue.
     */
    public void sendMessage(BaseMessage baseMessage) {
        mSendQueue.add(baseMessage.mSerialized);
    }

    /**
     * While channel is connected and stop is not called: * if sendQueue is not empty, then poll a message and send it * else if readChannel() is returning bytes, then add it to receivedQueue.
     */

    @Override
    public void run() {
        while (!this.isInterrupted()) {
            byte[] m;
            if ((m = mSendQueue.poll()) != null) {
                try {
                    mTcpChannel.write(m);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            byte[] br = null;
            try {
                br = readChannel();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (br != null) {
                mReceivedQueue.add(br);
            }
        }
    }

    /**
     * Kill the thread and close the channel.
     */
    public void stopSelf() throws IOException {
        this.interrupt();
        mConsumerThread.interrupt();
        mTcpChannel.closeChannel();
        //   mINetworkHandlerCallback.onSocketClosed();
    }

    /**
     * Try to read some bytes from the channel.
     */
    private byte[] readChannel() throws IOException {
        return mTcpChannel.read(1);
    }

    private class ReceivedMessageConsumer extends Thread {
        /**
         * While channel is connected and stop is not called:
         * if there exists message in receivedQueue,
         * then create a message object
         * from appropriate class based on message type byte and pass it through onMessageReceived
         * else if receivedQueue is empty, then sleep 100 ms.
         */
        @Override
        public void run() {
            while (!this.isInterrupted() && mTcpChannel.isConnected()) {
                if (!mReceivedQueue.isEmpty()) {
                    byte[] data = mReceivedQueue.poll();
                    ByteBuffer bb = ByteBuffer.wrap(data);
                    int length = bb.getInt();
                    byte protocol_version = bb.get();
                    byte messageType = bb.get();
                    BaseMessage message = null;
                    if (messageType == MessageTypes.STRING_MESSAGE) {
                        message = new StringMessage(data);
                    } else if (messageType == MessageTypes.JOIN_MESSAGE) {
                        message = new JoinMessage(data);
                    } else if (messageType == MessageTypes.READY_MESSAGE) {
                        message = new ReadyMessage(data);
                    } else if (messageType == MessageTypes.ATTACK_FEEDBACK_MESSAGE) {
                        message = new AttackFeedbackMessage(data);
                    } else if (messageType == MessageTypes.ATTACK_MESSAGE) {
                        message = new AttackMessage(data);
                    } else if (messageType == MessageTypes.LEAVE_MESSAGE) {
                        message = new LeaveMessage(data);
                    } else if (messageType == MessageTypes.START_GAME_MESSAGE) {
                        message = new StartGameMessage(data);
                    } else if (messageType == MessageTypes.TURN_MESSAGE) {
                        message = new TurnMessage(data);
                    } else if (messageType == MessageTypes.UNREADY_MESSAGE) {
                        message = new UnreadyMessage(data);
                    } else if (messageType == MessageTypes.REJECT_MESSAGE) {
                        message = new RejectMessage(data);
                    } else if (messageType == MessageTypes.VICTORY_MESSAGE) {
                        message = new VictoryMessage(data);
                    } else if (messageType == MessageTypes.CANCEL_MESSAGE) {
                        message = new CancelMessage(data);
                    }
                    mINetworkHandlerCallback.onMessageReceived(message);
                } else {
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        //   e.printStackTrace();
                    }
                }
            }
           if(!mTcpChannel.isConnected()){
               JOptionPane.showMessageDialog(Main.getBaseFrame(),new JLabel("connection has been lost"));
           }
        }
    }
}
