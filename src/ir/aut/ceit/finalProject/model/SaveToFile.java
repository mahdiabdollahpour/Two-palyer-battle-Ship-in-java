package ir.aut.ceit.finalProject.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.nio.ByteBuffer;
import java.security.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;


public class SaveToFile {
    // private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

    public static void saveJson(JSONObject jsonObject, boolean deleteOlder) throws IOException {
        FileOutputStream fos = null;
        try {

            File directory = new File("chathistory");
            if (!directory.exists()) {
                directory.mkdir();
                // If you require it to make the entire directory path including parents,
                // use directory.mkdirs(); here instead.
            }

            // Date date = new Date();

            File file = new File("chathistory" + "\\" + jsonObject.getString("id") + ".ser");
            if (deleteOlder) {
                file.delete();
            }
            fos = new FileOutputStream(file);
            fos.write(jsonObject.toString().getBytes());

        } catch (JSONException e) {
            e.printStackTrace();
        } finally {

            fos.close();
        }
    }

}
