package ir.aut.ceit.finalProject.view;


import javax.swing.*;
import java.awt.*;

public class Cell extends JLabel {

    private boolean mIsEmpty = true;
    private boolean mIsBorderOfShip = false;

    Cell(ImageIcon icon) {
        setIcon(icon);
    }

    public boolean isEmpty() {
        return mIsEmpty;
    }

    boolean isBorderOfShip() {
        return mIsBorderOfShip;
    }

    void setIsEmpty(boolean isEmpty) {
        mIsEmpty = isEmpty;
    }

    void setIsBorderOfShip(boolean isBorderOfShip) {
        mIsBorderOfShip = isBorderOfShip;
    }

    @Override
    public Dimension getPreferredSize() {
        return mDimension;
    }

    public static Dimension mDimension = new Dimension(50,50);
}


