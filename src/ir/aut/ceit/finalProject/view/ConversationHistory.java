package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.logic.Conversation;
import ir.aut.ceit.finalProject.model.ReadJson;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;

public class ConversationHistory extends JFrame {

    private static ConversationHistory instance;

    public static ConversationHistory getInstance() {
        if (instance == null) {
            instance = new ConversationHistory();
        }
        return instance;
    }

    private JPanel mContentPanel = new JPanel();
    private JScrollPane mScrollPane;

    private ConversationHistory() {
        super("Converstaion History");
        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        setSize(new Dimension(400, 1000));
    }

    public static void openConversation(Conversation conversation) {
        instance.getContentPane().removeAll();
        JTextArea textArea = new JTextArea();
        textArea.setEditable(false);
        for (int i = 0; i < conversation.getChat().size(); i++) {
            String s = null;
            try {
                s = conversation.getChat().get(i).getString("sender") + " : "
                        + conversation.getChat().get(i).getString("message") + "\n"
                        + conversation.getChat().get(i).getString("date");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            textArea.append(s + "\n");
        }
        instance.getContentPane().add(new JScrollPane(textArea));
        instance.getContentPane().revalidate();
        instance.getContentPane().repaint();

    }

    private static ArrayList<Conversation> readConversations() {
        ArrayList<JSONObject> arrayOfJsonedConversations = null;
        try {
            arrayOfJsonedConversations = ReadJson.readAllHistories();
        } catch (IOException e) {
            e.printStackTrace();
        }
        ArrayList<Conversation> chats = new ArrayList<>();

        for (int i = 0; i < arrayOfJsonedConversations.size(); i++) {
            JSONObject jsonObject = arrayOfJsonedConversations.get(i);
            Conversation conversation = null;
            try {
                conversation = Conversation.parseJson(jsonObject);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            chats.add(conversation);
        }
        System.out.println(chats.size() + "  chats");
        return chats;
    }

    public void showUp() {
        ArrayList<Conversation> chats = readConversations();
        mContentPanel.removeAll();
        getContentPane().removeAll();
        mContentPanel.setLayout(new BoxLayout(mContentPanel, BoxLayout.Y_AXIS));
        for (int i = chats.size() - 1; i >= 0; i--) {
            JLabel label = chats.get(i).makeLabel();
            mContentPanel.add(label);
        }
        mScrollPane = new JScrollPane(mContentPanel);
        getContentPane().add(mScrollPane);
        getInstance().setVisible(true);
    }

}
