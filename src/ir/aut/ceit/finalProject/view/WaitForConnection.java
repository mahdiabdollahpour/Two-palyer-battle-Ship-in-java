package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.logic.RequestRemoveListener;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by ASUS on 04/07/2017.
 */

public class WaitForConnection extends JFrame {
    private static WaitForConnection instance;
    private static ArrayList<ConnectionRequest> requests = new ArrayList<>();
    private static RequestRemoveListener mRequestRemoveListener = new RequestRemoveListener() {
        @Override
        public void remove(ConnectionRequest connectionRequest) {
            instance.mContentPanel.remove(connectionRequest);
            requests.remove(connectionRequest);
            instance.mContentPanel.revalidate();
            instance.mContentPanel.repaint();
        }
    };
    private JScrollPane mScrollPane;
    private JPanel mContentPanel = new JPanel();

    public static void makeRequest(ConnectionRequest connectionRequest) {
        System.out.println("making request");
        connectionRequest.setmRequestRemoveListener(mRequestRemoveListener);
        requests.add(connectionRequest);
        instance.mContentPanel.add(connectionRequest);
        instance.repaint();
        instance.revalidate();
    }

    public static void cancelRequest(String ip) {
        for (int i = 0; i < requests.size(); i++) {
            if (requests.get(i).getIP().equals(ip)) {
                requests.get(i).getRejectButton().doClick();
            }
        }
    }

    public Dimension getPreferredSize() {
        return new Dimension(400, 200);
    }

    public Dimension getMaximumSize() {
        return new Dimension(400, 200);
    }

    public static WaitForConnection getInstance() {
        if (instance == null) {
            instance = new WaitForConnection();
        }

        return instance;
    }

    private WaitForConnection() {
        setTitle("Waiting for connections");
        Dimension d = new Dimension(400, 800);
        setSize(d);
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);


        mContentPanel.setPreferredSize(d);
        mContentPanel.setMinimumSize(d);
        mContentPanel.setLayout(new BoxLayout(mContentPanel, BoxLayout.Y_AXIS));
        mScrollPane = new JScrollPane(mContentPanel);
        getContentPane().add(mScrollPane);

        for (int i = 0; i < requests.size(); i++) {
            mContentPanel.add(requests.get(i));
        }

    }


}




