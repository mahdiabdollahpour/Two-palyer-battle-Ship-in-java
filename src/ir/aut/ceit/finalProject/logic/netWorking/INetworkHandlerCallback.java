package ir.aut.ceit.finalProject.logic.netWorking;


public interface INetworkHandlerCallback {
    void onMessageReceived(BaseMessage baseMessage);
    void onSocketClosed();
}