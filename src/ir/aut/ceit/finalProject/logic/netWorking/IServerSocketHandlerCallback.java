package ir.aut.ceit.finalProject.logic.netWorking;

public interface IServerSocketHandlerCallback {
    void onNewConnectionReceived(NetworkHandler networkHandler);
}