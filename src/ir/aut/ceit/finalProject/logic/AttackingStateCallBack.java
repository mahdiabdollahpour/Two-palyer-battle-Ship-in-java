package ir.aut.ceit.finalProject.logic;

/**
 * Created by ASUS on 08/07/2017.
 */
public interface AttackingStateCallBack {
    void sendAttackMessage(int x, int y) ;
}
