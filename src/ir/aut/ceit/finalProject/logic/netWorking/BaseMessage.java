package ir.aut.ceit.finalProject.logic.netWorking;

public abstract class BaseMessage {
    protected byte[] mSerialized;

    protected abstract void serialize();

    protected abstract void deserialize();

    public abstract byte getMessageType();

    public byte[] getmSerialized() {
        return mSerialized;
    }

}
