package ir.aut.ceit.finalProject.view;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class PanelOfGameForSettingShips extends JPanel {

    private Cell[][] cells = new Cell[10][10];
    private SetEventHandler[][] eventHandler = new SetEventHandler[10][10];
    private String selectedShip;
    private ImageIcon ship = new ImageIcon("ship.PNG");
    private ImageIcon emptyCell = new ImageIcon("emptyCell.PNG");
    private FooterForChoosingShip fp;
    private ArrayList<Ship> ships = new ArrayList<>();
    private String direction = "noDirection";

    public PanelOfGameForSettingShips() {
        setLayout(new GridBagLayout());
        this.setPreferredSize(new Dimension(10 * Cell.mDimension.width, 10 * Cell.mDimension.height));

        GridBagConstraints gbc = new GridBagConstraints();
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                if (j == 0) {
                    gbc.gridx = i + 1;
                    gbc.gridy = j;
                    JLabel label = new JLabel((i + 1) + "");
                    add(label, gbc);
                }
                if (i == 0) {
                    gbc.gridy = j + 1;
                    gbc.gridx = i;
                    JLabel label = new JLabel((char) (j + 65) + "");
                    add(label, gbc);
                }
                gbc.gridy = j + 1;
                gbc.gridx = i + 1;
                cells[i][j] = new Cell(emptyCell);
                add(cells[i][j], gbc);
            }
    }

    void setShipsOnMap(FooterForChoosingShip fp) {
        this.selectedShip = fp.getSelectedItem().toString();
        this.fp = fp;
        //   JOptionPane.showMessageDialog(this, "Now select the Cell:", "Put Ship", JOptionPane.PLAIN_MESSAGE);
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                eventHandler[i][j] = new SetEventHandler(i, j);
            }

        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                cells[i][j].addMouseListener(eventHandler[i][j]);
            }
    }

    boolean setShipsOnMap(Ship ship) {
        this.direction = ship.getDirection();
        this.selectedShip = ship.getKind();
        return selectPosition(selectedShip, ship.getX(), ship.getY());
    }

    private class SetEventHandler extends MouseAdapter {

        int xPosition;
        int yPosition;

        SetEventHandler(int x, int y) {
            xPosition = x;
            yPosition = y;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);
            if (cells[xPosition][yPosition].isEmpty()) {
                if (!selectPosition(selectedShip, xPosition, yPosition)) {
                    JOptionPane.showMessageDialog(null, "Cannot put ship here.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "This cell is not Empty.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private boolean selectPosition(String selectedShip, int xPosition, int yPosition) {
        int lengthOfShip = getLengthOfShip(selectedShip);

        if ((direction.equals("right") || direction.equals("noDirection")) && selectRightDirection(xPosition, yPosition, selectedShip)) {
            setShipsInRightDirection(lengthOfShip, xPosition, yPosition);
            addShipsToArrayList(selectedShip, "right", xPosition, yPosition);
            fp.setNumberOfShipsInMap();
            return true;
        } else if ((direction.equals("down") || direction.equals("noDirection")) && selectDownDirection(xPosition, yPosition, selectedShip)) {
            setShipsInDownDirection(lengthOfShip, xPosition, yPosition);
            addShipsToArrayList(selectedShip, "down", xPosition, yPosition);
            fp.setNumberOfShipsInMap();
            return true;
        } else if ((direction.equals("left") || direction.equals("noDirection")) && selectLeftDirection(xPosition, yPosition, selectedShip)) {
            setShipsInLeftDirection(lengthOfShip, xPosition, yPosition);
            addShipsToArrayList(selectedShip, "left", xPosition, yPosition);
            fp.setNumberOfShipsInMap();
            return true;
        } else if ((direction.equals("up") || direction.equals("noDirection")) && selectUpDirection(xPosition, yPosition, selectedShip)) {
            setShipsInUpDirection(lengthOfShip, xPosition, yPosition);
            addShipsToArrayList(selectedShip, "up", xPosition, yPosition);
            fp.setNumberOfShipsInMap();
            return true;
        }
        return false;
    }

    private void addShipsToArrayList(String selectedShip, String direction, int x, int y) {
        Ship shipObject = new Ship(selectedShip, getLengthOfShip(selectedShip), direction, x, y);
        ships.add(shipObject);
    }

    private void setShipsInRightDirection(int lengthOfShip, int xPosition, int yPosition) {
        for (int i = yPosition; i < yPosition + lengthOfShip; i++) {
            putShipInRightORLeftDirection(xPosition, i);
        }
        for (int i = xPosition - 1; i <= xPosition + 1; i++) {
            try {
                cells[i][yPosition - 1].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                cells[i][yPosition + lengthOfShip].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
        removeEventHandler();
        fp.setSelectedItem(new StringBuffer("nothing"));
    }

    private void setShipsInLeftDirection(int lengthOfShip, int xPosition, int yPosition) {
        for (int i = yPosition; i > yPosition - lengthOfShip; i--) {
            putShipInRightORLeftDirection(xPosition, i);
        }
        for (int i = xPosition - 1; i <= xPosition + 1; i++) {
            try {
                cells[i][yPosition + 1].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                cells[i][yPosition - lengthOfShip].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
        removeEventHandler();
        fp.setSelectedItem(new StringBuffer("nothing"));
    }

    private void putShipInRightORLeftDirection(int xPosition, int i) {
        cells[xPosition][i].setIcon(ship);
        cells[xPosition][i].setIsEmpty(false);
        cells[xPosition][i].setIsBorderOfShip(true);
        try {
            cells[xPosition + 1][i].setIsBorderOfShip(true);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            cells[xPosition - 1][i].setIsBorderOfShip(true);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
    }

    private void setShipsInDownDirection(int lengthOfShip, int xPosition, int yPosition) {
        for (int i = xPosition; i < xPosition + lengthOfShip; i++) {
            putShipsInDownORUpDirection(yPosition, cells[i]);
        }
        for (int i = yPosition - 1; i < yPosition + 1; i++) {
            try {
                cells[xPosition - 1][i].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                cells[xPosition + lengthOfShip][i].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
        removeEventHandler();
        fp.setSelectedItem(new StringBuffer("nothing"));
    }

    private void setShipsInUpDirection(int lengthOfShip, int xPosition, int yPosition) {
        for (int i = xPosition; i > xPosition - lengthOfShip; i--) {
            putShipsInDownORUpDirection(yPosition, cells[i]);
        }
        for (int i = yPosition - 1; i < yPosition + 1; i++) {
            try {
                cells[xPosition + 1][i].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                cells[xPosition - lengthOfShip][i].setIsBorderOfShip(true);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
        removeEventHandler();
        fp.setSelectedItem(new StringBuffer("nothing"));
    }

    private void putShipsInDownORUpDirection(int yPosition, Cell[] cell) {
        cell[yPosition].setIcon(ship);
        cell[yPosition].setIsEmpty(false);
        cell[yPosition].setIsBorderOfShip(true);
        try {
            cell[yPosition - 1].setIsBorderOfShip(true);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            cell[yPosition + 1].setIsBorderOfShip(true);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
    }

    private boolean selectRightDirection(int x, int y, String kind) {
        boolean isEmpty = false;
        int lengthOfShip = getLengthOfShip(kind);

        if (y + lengthOfShip - 1 <= 9) {
            isEmpty = isEmptyForRightDirection(cells[x], y, false, y + lengthOfShip);
        }

        if (isEmpty && x > 0) {
            for (int i = y; i <= y + lengthOfShip; i++) {
                try {
                    if (!cells[x - 1][i].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }

        if (isEmpty && x < 9) {
            for (int i = y - 1; i <= y + lengthOfShip; i++) {
                try {
                    if (!cells[x + 1][i].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }
        try {
            if (!cells[x][y - 1].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            if (!cells[x][y + lengthOfShip].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        return isEmpty;
    }

    private boolean isEmptyForRightDirection(Cell[] cell, int y, boolean isEmpty, int i1) {
        for (int i = y; i < i1; i++) {
            isEmpty = true;
            try {
                if (!cell[i].isEmpty() || cell[i].isBorderOfShip()) {
                    isEmpty = false;
                    break;
                }
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
        return isEmpty;
    }

    private boolean selectDownDirection(int x, int y, String kind) {
        boolean isEmpty = false;
        int lengthOfShip = getLengthOfShip(kind);

        if (x + lengthOfShip - 1 <= 9) {
            for (int i = x; i < x + lengthOfShip; i++) {
                isEmpty = true;
                try {
                    if (!cells[i][y].isEmpty() || cells[i][y].isBorderOfShip()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }

        if (isEmpty && y > 0) {
            for (int i = x - 1; i <= x + lengthOfShip; i++) {
                try {
                    if (!cells[i][y - 1].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }

        if (isEmpty && y < 9) {
            for (int i = x - 1; i <= x + lengthOfShip; i++) {
                try {
                    if (!cells[i][y + 1].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }
        try {
            if (!cells[x - 1][y].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            if (!cells[x + lengthOfShip][y].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        return isEmpty;
    }

    private boolean selectLeftDirection(int x, int y, String kind) {
        boolean isEmpty = false;
        int lengthOfShip = getLengthOfShip(kind);
        if (y - lengthOfShip + 1 >= 0) {
            isEmpty = isEmptyforLeftDirection(cells[x], y, false, y - lengthOfShip);
        }

        if (isEmpty && x > 0) {
            for (int i = y + 1; i >= y - lengthOfShip; i--) {
                try {
                    if (!cells[x - 1][i].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }

        if (isEmpty && x < 9) {
            for (int i = y + 1; i >= y - lengthOfShip; i--) {
                try {
                    if (!cells[x + 1][i].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }

        try {
            if (!cells[x][y + 1].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            if (!cells[x][y - lengthOfShip].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        return isEmpty;
    }

    private boolean isEmptyforLeftDirection(Cell[] cell, int y, boolean isEmpty, int i1) {
        for (int i = y; i > i1; i--) {
            isEmpty = true;
            try {
                if (!cell[i].isEmpty() || cell[i].isBorderOfShip()) {
                    isEmpty = false;
                    break;
                }
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
        return isEmpty;
    }

    private boolean selectUpDirection(int x, int y, String kind) {
        boolean isEmpty = false;
        int lengthOfShip = getLengthOfShip(kind);

        if (x - lengthOfShip + 1 >= 0) {
            for (int i = x; i > x - lengthOfShip; i--) {
                isEmpty = true;
                try {
                    if (!cells[i][y].isEmpty() || cells[i][y].isBorderOfShip()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }

        if (isEmpty && y > 0) {
            for (int i = x + 1; i >= x - lengthOfShip; i--) {
                try {
                    if (!cells[i][y + 1].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }

        if (isEmpty && y < 9) {
            for (int i = x + 1; i >= x - lengthOfShip; i--) {
                try {
                    if (!cells[i][y + 1].isEmpty()) {
                        isEmpty = false;
                        break;
                    }
                } catch (ArrayIndexOutOfBoundsException ignored) {
                }
            }
        }
        try {
            if (!cells[x + 1][y].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            if (!cells[x - lengthOfShip][y].isEmpty()) {
                isEmpty = false;
            }
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        return isEmpty;
    }

    private int getLengthOfShip(String kind) {
        switch (kind) {
            case "navy":
                return 4;
            case "ship":
                return 3;
            case "cruiser":
                return 2;
            default:  // "submarine"
                return 1;
        }
    }

    private void removeEventHandler() {
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                cells[i][j].removeMouseListener(eventHandler[i][j]);
            }

        }
    }

    ArrayList<Ship> getShips() {
        return ships;
    }

    public Cell[][] getCells() {
        return cells;
    }

    ImageIcon getEmptyCell() {
        return emptyCell;
    }

    void setDirection() {
        this.direction = "noDirection";
    }


}
