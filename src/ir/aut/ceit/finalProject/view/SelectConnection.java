package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.logic.SelectConnectionModeCallback;

import javax.swing.*;
import java.awt.*;

public class SelectConnection extends JFrame {

    private SelectConnection thisOne = this;
    private boolean isConnected;

    public SelectConnection(SelectConnectionModeCallback selectConnectionModeCallback) {
        super("Select Connection Mode");
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        JLabel name = new JLabel("Name");
        JLabel port1 = new JLabel("Port");
        JLabel port2 = new JLabel("Port");
        JLabel IP = new JLabel("IP   ");
        JTextField nameField = new JTextField(10);
        JTextField portFieldHost = new JTextField(10);
        JTextField portFieldGuest = new JTextField(10);
        JTextField IPField = new JTextField(15);
        JButton exit = new JButton("Exit");
        exit.addActionListener(e -> System.exit(0));
        JButton start = new JButton("Start");
        JRadioButton guest = new JRadioButton("guest");
        JRadioButton host = new JRadioButton("host");
        start.addActionListener(e -> {
            if (host.isSelected()) {
                Main.isHost();
                selectConnectionModeCallback.connectAsHost(portFieldHost.getText(), nameField.getText());
            } else if (guest.isSelected()) {
                thisOne.dispose();
                selectConnectionModeCallback.connectAsGuest(IPField.getText(), portFieldGuest.getText(), nameField.getText());
            }
            thisOne.dispose();
        });
        ButtonGroup group = new ButtonGroup();
        group.add(guest);
        group.add(host);

        gbc.gridx = 0;
        gbc.gridy = 0;
        add(name, gbc);
        gbc.gridx = 1;
        add(nameField, gbc);
        gbc.gridy = 1;
        gbc.gridx = 0;
        add(host, gbc);

        gbc.gridy = 2;
        add(port1, gbc);
        gbc.gridx = 1;
        add(portFieldHost, gbc);

        gbc.gridy = 3;
        gbc.gridx = 0;
        add(guest, gbc);

        gbc.gridy = 4;
        add(port2, gbc);
        gbc.gridx = 1;
        add(portFieldGuest, gbc);
        gbc.gridx = 2;
        add(IP, gbc);
        gbc.gridx = 3;
        add(IPField, gbc);


        gbc.gridy = 5;
        gbc.gridx = 2;
        add(exit, gbc);
        gbc.gridx = 1;
        add(start, gbc);


        setSize(new Dimension(450, 500));
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setVisible(true);
    }

}
