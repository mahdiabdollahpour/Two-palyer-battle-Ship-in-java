package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

public class RejectMessage extends BaseMessage {


    public RejectMessage(byte[] data) {
        mSerialized = data;
        deserialize();
    }

    public RejectMessage() {
        serialize();
    }

    @Override
    public void serialize() {
        int messageLength = 4 + 1 + 1;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.REJECT_MESSAGE);
        mSerialized = byteBuffer.array();


    }

    private byte messageType;

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        messageType = byteBuffer.get();
    }

    @Override
    public byte getMessageType() {
        return messageType;
    }


}
