package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

public class AttackMessage extends BaseMessage {

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    public AttackMessage(int x, int y) {
        this.mX = x;
        this.mY = y;
        serialize();
    }

    public AttackMessage(byte[] serialized) {
        mSerialized = serialized;
        deserialize();
    }

    int mX;
    int mY;

    @Override
    public void serialize() {
        int messageLength = 4 + 4 + 1 + 1 + 4;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.ATTACK_MESSAGE);
        byteBuffer.putInt(mX);
        byteBuffer.putInt(mY);
        mSerialized = byteBuffer.array();
    }

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        mX = byteBuffer.getInt();
        mY = byteBuffer.getInt();
    }

    @Override
    public byte getMessageType() {
        return MessageTypes.ATTACK_MESSAGE;
    }
}
