package ir.aut.ceit.finalProject;

import ir.aut.ceit.finalProject.logic.SelectConnectionModeCallback;
import ir.aut.ceit.finalProject.logic.SelectTurnCallBack;
import ir.aut.ceit.finalProject.logic.StringListener;
import ir.aut.ceit.finalProject.logic.netWorking.MessageManager;
import ir.aut.ceit.finalProject.view.*;

import javax.swing.*;

import java.awt.*;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.Random;


public class Main {
    private static String mName;
    private static MessageManager messageManager = null;
    private static PanelOfGameForSettingShips cells;
    private static PanelOfGameForAttacking cellsOfApponent;
    private static JFrame baseFrame;
    private static String mOpponentName;
    private static String mOpponentIP;
    private static ChatBox chatBox;
    private static FooterForChoosingShip footerForChoosingShip;
    private static ImageIcon shuttedShip = new ImageIcon("shuttedShip.PNG");
    private static ImageIcon emptyShuttedCell = new ImageIcon("EmptyShuttedCell.PNG");
    private static JPanel panelOfCells;
    private static SelectTurnCallBack turnCallBack = null;
    private static int numberOfFullCells;
    private static boolean isHost = false;

    private static SelectConnectionModeCallback mSelectConnectionModeCallback = new SelectConnectionModeCallback() {
        @Override
        public void connectAsHost(String port, String name) {
            Main.mName = name;
            messageManager = new MessageManager(Integer.parseInt(port));
            WaitForConnection.getInstance().setVisible(true);
        }

        @Override
        public void connectAsGuest(String IP, String port, String name) {
            Main.mName = name;
            //      "127.0.0.1"
            Main.setOpponentIP(IP);
            WaitForHostToJoin.getInstance().getWaitingState();
            WaitForHostToJoin.getInstance().setVisible(true);
            messageManager = new MessageManager(IP, Integer.parseInt(port));
            WaitForHostToJoin.getInstance().setCancelAction(messageManager);
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            messageManager.sendJoinMessage(name, setMyIP());
        }
    };

    public static void main(String[] args) throws InterruptedException {

        runSelectConnection();
    }

    public static void runSelectConnection() {

        new SelectConnection(mSelectConnectionModeCallback);
    }

    public static void startGame() {
        numberOfFullCells = 10;
        chatBox = new ChatBox(messageManager.getCallBackFromMessageManagerToChatBox());
        cells = new PanelOfGameForSettingShips();
        footerForChoosingShip = new FooterForChoosingShip(cells, messageManager);
        cellsOfApponent = new PanelOfGameForAttacking(messageManager);
        baseFrame = new JFrame();
        baseFrame.setLayout(new BorderLayout());
        JMenuBar menuBar = new JMenuBar();
        JMenu file = new JMenu("File");
        JMenu help = new JMenu("Help");
        JMenuItem conversationHistory = new JMenuItem("Conversation History");
        conversationHistory.addActionListener(e -> ConversationHistory.getInstance().showUp());
        file.add(conversationHistory);
        menuBar.add(file);
        menuBar.add(help);
        baseFrame.setJMenuBar(menuBar);
        baseFrame.setTitle(mName);
        menuBar.setVisible(true);
        setCells();
        setChatBox();
        setFooterForChoosingShip();
        baseFrame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        baseFrame.setSize(1000, 1000);
        baseFrame.setVisible(true);
        turnCallBack = messageManager;
        if (isHost) {
            Random random = new Random();
            int yourTurn = 0;
            int opponentTurn = 0;
            while ((yourTurn == 1 && opponentTurn == 1) || (yourTurn == 0 && opponentTurn == 0)){
                yourTurn = random.nextInt(2);
                opponentTurn = random.nextInt(2);
            }
            System.out.println(yourTurn + "," + opponentTurn);
            turnCallBack.sendTurnMessage(yourTurn, opponentTurn);
        }
    }

    private static void setCells() {
        panelOfCells = new JPanel();
        baseFrame.add(panelOfCells, BorderLayout.CENTER);
        panelOfCells.add(cells);
    }

    private static void setChatBox() {
        baseFrame.add(chatBox, BorderLayout.EAST);
    }

    private static void setFooterForChoosingShip() {
        baseFrame.add(footerForChoosingShip, BorderLayout.SOUTH);
    }

    public static void removeFooterForChoosingShip() {
        baseFrame.remove(footerForChoosingShip);
        baseFrame.add(new FooterWhenAttack(messageManager), BorderLayout.SOUTH);
        baseFrame.setVisible(true);
    }

    public static JFrame getBaseFrame() {
        return baseFrame;
    }

    public static PanelOfGameForSettingShips getCells() {
        return cells;
    }

    public static void replaceCellsWithCellsOfApponent() {
        panelOfCells.remove(cells);
        panelOfCells.add(cellsOfApponent);
        baseFrame.setVisible(false);
        baseFrame.setVisible(true);
    }

    private static void replaceCellsOfApponentWithCells() {
        panelOfCells.remove(cellsOfApponent);
        panelOfCells.add(cells);
        baseFrame.setVisible(false);
        baseFrame.setVisible(true);
    }

    public static void shutCellOfOpponent(int x, int y) {
        cellsOfApponent.getCells()[x][y].setIcon(shuttedShip);
        baseFrame.setVisible(true);
    }

    public static void emptyCellOfOpponent(int x, int y) {
        cellsOfApponent.getCells()[x][y].setIcon(emptyShuttedCell);
        baseFrame.setVisible(true);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        replaceCellsOfApponentWithCells();
    }

    public static void shutCellOfYou(int x, int y) {
        cells.getCells()[x][y].setIcon(shuttedShip);
        baseFrame.setVisible(true);
        numberOfFullCells--;
        if (numberOfFullCells == 0) {
            JLabel label = new JLabel();
            label.setIcon(new ImageIcon("gameOver.png"));
            messageManager.sendVictoryMessage();
            JOptionPane.showMessageDialog(baseFrame, label, "GAME OVER", JOptionPane.PLAIN_MESSAGE);
        }
    }

    public static void emptyCellOfYou(int x, int y) {
        cells.getCells()[x][y].setIcon(emptyShuttedCell);
        baseFrame.setVisible(true);
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        replaceCellsWithCellsOfApponent();
    }

    public static String getOpponentName() {
        return mOpponentName;
    }

    public static void setOpponentName(String opponentName) {
        Main.mOpponentName = opponentName;
    }

    public static String getOpponentIP() {
        return mOpponentIP;
    }

    public static void setOpponentIP(String opponentIP) {
        Main.mOpponentIP = opponentIP;
    }

    public static String getName() {
        return mName;
    }

    public static void setStringListener(StringListener stringListener) {
        messageManager.setChatBoxCallback(stringListener);
    }

    static class FooterWhenAttack extends JPanel {
        private JButton leave;
        private Dimension d;

        public FooterWhenAttack(ActionListener leaveAction) {
            System.out.println("making foote for attack");
            //  setBorder(new LineBorder(Color.MAGENTA, 10));
            leave = new JButton("Leave");
            leave.setMinimumSize(new Dimension(50, 50));
            add(leave);
            leave.addActionListener(leaveAction);
            d = new Dimension(500, 100);
            setSize(d);
        }

        @Override
        public Dimension getMinimumSize() {
            return d;
        }

        public Dimension getPreferredSize() {
            return d;

        }
    }

    private static String mYourIp = null;

    public static String setMyIP() {
        if (mYourIp != null) {
            return mYourIp;
        }
        String s = null;
        InetAddress ip;
        try {

            ip = InetAddress.getLocalHost();
            s = ip.getHostAddress();
            System.out.println("Current IP address : " + s);

        } catch (UnknownHostException e) {

            e.printStackTrace();

        }

        mYourIp = s;
        return s;
    }

    public static void isHost() {
        isHost = true;
    }

}
