package ir.aut.ceit.finalProject.logic;

public interface SelectTurnCallBack {
    void sendTurnMessage(int yours, int opponents);
}
