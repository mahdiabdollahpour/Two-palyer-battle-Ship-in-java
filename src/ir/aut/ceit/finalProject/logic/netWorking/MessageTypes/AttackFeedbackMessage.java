package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

public class AttackFeedbackMessage extends BaseMessage {


    public AttackFeedbackMessage(byte[] data) {
        mSerialized = data;
        deserialize();
    }

    public int getX() {
        return mX;
    }

    public int getY() {
        return mY;
    }

    public byte getFeedback() {
        return mFeedback;
    }

    private int mX;

    private int mY;
    private byte mFeedback;

    public AttackFeedbackMessage(int x, int y, byte feedback) {
        this.mX = x;
        this.mY = y;
        this.mFeedback = feedback;
        serialize();
    }

    class FeedbackTypes {
        static final byte SUCCESSFULL = 1;
        static final byte FAILED = 0;
    }


    @Override
    public void serialize() {
        int messageLength = 4 + 4 + 1 + 1 + 4 + 1;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.ATTACK_FEEDBACK_MESSAGE);
        byteBuffer.putInt(mX);
        byteBuffer.putInt(mY);
        byteBuffer.put(mFeedback);
        mSerialized = byteBuffer.array();
    }

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        mX = byteBuffer.getInt();
        mY = byteBuffer.getInt();
        mFeedback = byteBuffer.get();
    }

    @Override
    public byte getMessageType() {
        return MessageTypes.ATTACK_FEEDBACK_MESSAGE;
    }
}
