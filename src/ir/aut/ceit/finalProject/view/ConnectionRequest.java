package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.logic.RequestConnectionCallback;
import ir.aut.ceit.finalProject.logic.RequestRemoveListener;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ConnectionRequest extends JPanel {
    private String mName;
    private String mIP;
    private JButton mAccept = new JButton("Accept");
    private JButton mReject = new JButton("Reject");
    private ActionListener mRejectAction;

    public String getIP() {
        return mIP;
    }

    public JButton getRejectButton() {
        return mReject;
    }

    private ConnectionRequest innerClass = this;
    private RequestRemoveListener mRequestRemoveListener;

    public void setRequestConnectionListener(RequestConnectionCallback RequestConnectionCallback) {
        this.mRequestConnectionCallback = RequestConnectionCallback;
    }

    private RequestConnectionCallback mRequestConnectionCallback;
    private static int indexTillNow = 0;

    public static void setIndexingToZero() {
        indexTillNow = 0;
    }

    public int getIndex() {
        return mIndex;
    }

    public ActionListener getRejectAction() {
        return mRejectAction;
    }

    private int mIndex;

    public void setmRequestRemoveListener(RequestRemoveListener requestRemoveListener) {
        mRequestRemoveListener = requestRemoveListener;
    }

    public ConnectionRequest(String name, String IP) {
        mIndex = indexTillNow;
        indexTillNow++;
        mIP = IP;
        mName = name;
        mRejectAction = e -> {
            mRequestRemoveListener.remove(innerClass);
            mRequestConnectionCallback.rejectPerformed(mIndex);
        };
        mReject.addActionListener(mRejectAction);

        mAccept.addActionListener(e -> {
            WaitForConnection.getInstance().dispose();
            System.out.println("mIndex :" + mIndex);
            mRequestConnectionCallback.acceotPerformed(mIndex);

        });

        setLayout(new GridBagLayout());
        setBorder(new LineBorder(Color.black, 5));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(new JLabel(mName), gbc);
        gbc.gridy = 1;
        add(new JLabel(mIP), gbc);
        gbc.gridy = 2;
        add(mAccept, gbc);
        gbc.gridx = 2;
        add(mReject, gbc);

    }

    @Override
    public Dimension getMaximumSize() {
        return new Dimension(400, 100);
    }


}
