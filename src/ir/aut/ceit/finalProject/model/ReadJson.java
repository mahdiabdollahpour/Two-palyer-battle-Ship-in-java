package ir.aut.ceit.finalProject.model;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by ASUS on 06/07/2017.
 */
public class ReadJson {
    public static ArrayList<JSONObject> readAllHistories() throws IOException {
        ArrayList<JSONObject> arrayList = new ArrayList<>();
        File directory = new File("chathistory");
        for (int i = 0; i < directory.listFiles().length; i++) {
            File file = directory.listFiles()[i];

            if (file.getName().endsWith(".ser")) {
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    int len = fis.available();
                    byte[] data = new byte[len];
                    fis.read(data);
                    String s = new String(data);
                    JSONObject jsonObject = new JSONObject(s);
                    arrayList.add(jsonObject);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }finally {
                    fis.close();
                }
            }
        }
        return arrayList;
    }

    public static JSONObject readHistoryWithID(String ID) throws IOException {
        File file = new File("chathistory" + "\\" + ID + ".ser");
        JSONObject jsonObject = null;
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(file);
            int len = fis.available();
            byte[] data = new byte[len];
            fis.read(data);
            String s = new String(data);
            jsonObject = new JSONObject(s);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }finally {
            fis.close();
        }

        return jsonObject;

    }

}
