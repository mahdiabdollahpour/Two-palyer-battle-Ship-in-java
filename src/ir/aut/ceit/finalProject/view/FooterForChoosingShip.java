package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.logic.ReadyAndUnreadyCallback;
import ir.aut.ceit.finalProject.logic.netWorking.MessageManager;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class FooterForChoosingShip extends JPanel {
    private JButton navy = new JButton("NAVY");
    private JButton ship = new JButton("SHIP");
    private JButton cruiser = new JButton("CRUISER");
    private JButton subMarine = new JButton("SUBMARINE");
    private JButton rotationButton = new JButton();
    private JButton resetButton = new JButton("Reset");
    private JButton readyButton = new JButton("Ready");
    private int numberOfNavy = 1;
    private int numberOfShip = 1;
    private int numberOfCruiser = 1;
    private int numberOfSubmarine = 1;
    private AbstractAction abstractAction;
    private ActionListener actionForResetButton;
    private ActionListener actionForCancelButton;
    private ActionListener actionForReadyButton;
    private ReadyAndUnreadyCallback mReadyAndUnreadyCallback;

    private SetActionForKeys action;

    private PanelOfGameForSettingShips panelOfGameForSettingShips;

    private StringBuffer selectedItem = new StringBuffer("nothing");

    public FooterForChoosingShip(PanelOfGameForSettingShips p, ReadyAndUnreadyCallback readyAndUnreadyCallback) {
       mReadyAndUnreadyCallback = readyAndUnreadyCallback;
        this.panelOfGameForSettingShips = p;
        setButtonsOnPanel();
        setActionForActionAndReady();
        addActionHandler("for all keys");
    }

    private void setButtonsOnPanel() {
        setLayout(new GridBagLayout());
        GridBagConstraints gb = new GridBagConstraints();
        gb.gridx = 0;
        gb.gridy = 1;
        gb.weightx = 0;
        gb.weighty = 0;
        add(navy, gb);
        gb.gridx = 0;
        gb.gridy = 2;
        add(ship, gb);
        gb.gridx = 0;
        gb.gridy = 3;
        add(cruiser, gb);
        gb.gridx = 0;
        gb.gridy = 4;
        add(subMarine, gb);
        gb.gridx = 3;
        gb.gridy = 2;
        add(rotationButton, gb);
        gb.gridx = 3;
        gb.gridy = 3;
        gb.weightx = 1;
        gb.weighty = 0;
        add(resetButton, gb);
        gb.gridx = 3;
        gb.gridy = 4;
        gb.weightx = 0;
        add(readyButton, gb);
    }

    private void setActionForActionAndReady() {
        action = new SetActionForKeys(this);
        addActionForCancelButton();
        addActionForReadyButton();
    }

    private void addActionForReadyButton() {
        actionForReadyButton = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("from actionPerformed in ready");
                if (isMapFull()) {
                    readyButton.removeActionListener(this);
                   mReadyAndUnreadyCallback.sendReadyMessage();
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e1) {
                        e1.printStackTrace();
                    }
                    if (MessageManager.isOpponentReady()) {
                        Main.removeFooterForChoosingShip();
                        JOptionPane.showMessageDialog(Main.getBaseFrame(), "Now let's play!!!");
                        if(MessageManager.ismYourTurn()){
                            Main.replaceCellsWithCellsOfApponent();
                        }
                    } else {
                        readyButton.setText("Cancel");
                        removeActionsForKeys("All of keys");
                        Main.getBaseFrame().setVisible(true);
                        readyButton.addActionListener(actionForCancelButton);
                    }
                    rotationButton.setIcon(new ImageIcon("rotation.png"));
                } else {
                    JOptionPane.showMessageDialog(null, "You haven't used all of your ship.", "Error", JOptionPane.ERROR_MESSAGE);
                }
            }
        };
    }

    private void addActionForCancelButton() {
        actionForCancelButton = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("we are in cancel");
                readyButton.removeActionListener(this);
                System.out.println("we are in cancel");
                addActionHandler("for all keys");
                readyButton.setText("Ready");
            mReadyAndUnreadyCallback.sendUnreadyMessage();
                Main.getBaseFrame().setVisible(true);
            }
        };
    }

    private void addActionHandler(String isForShipsOnly) {
        navy.addActionListener(action);
        ship.addActionListener(action);
        cruiser.addActionListener(action);
        subMarine.addActionListener(action);
        if (!isForShipsOnly.equals("It is only for ships")) {
            setActionForRotation();
            readyButton.addActionListener(actionForReadyButton);
            rotationButton.setIcon(new ImageIcon("rotation.png"));
            navy.setIcon(new ImageIcon("navy.png"));
            ship.setIcon(new ImageIcon("ships.png"));
            cruiser.setIcon(new ImageIcon("cruiser.png"));
            subMarine.setIcon(new ImageIcon("submarine.png"));
        }
    }

    private void removeActionsForKeys(String isSelectingShip) {
        navy.removeActionListener(action);
        cruiser.removeActionListener(action);
        ship.removeActionListener(action);
        subMarine.removeActionListener(action);
        if (!isSelectingShip.equals("just keys of selecting ship")) {
            rotationButton.removeActionListener(abstractAction);
            resetButton.removeActionListener(actionForResetButton);
        }
    }

    private boolean isMapFull() {
        System.out.println(panelOfGameForSettingShips.getShips().size());
        return panelOfGameForSettingShips.getShips().size() == 4;
    }

    private class SetActionForKeys implements ActionListener {

        FooterForChoosingShip fp;

        SetActionForKeys(FooterForChoosingShip fp) {
            this.fp = fp;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(e.getActionCommand());
            if (e.getActionCommand().equals("NAVY")) {
                if (numberOfNavy > 0) {
                    removeActionsForKeys("just keys of selecting ship");
                    selectedItem.delete(0, selectedItem.length());
                    selectedItem.append("navy");
                    panelOfGameForSettingShips.setShipsOnMap(fp);
                    addActionHandler("It is only for ships");
                } else {
                    JOptionPane.showMessageDialog(null, "Can't use navy any more!", "Usage Limitation", JOptionPane.ERROR_MESSAGE);
                }
            }
            if (e.getActionCommand().equals("SHIP")) {
                if (numberOfShip > 0) {
                    removeActionsForKeys("just keys of selecting ship");
                    selectedItem.delete(0, selectedItem.length());
                    selectedItem.append("ship");
                    panelOfGameForSettingShips.setShipsOnMap(fp);
                    addActionHandler("It is only for ships");
                } else {
                    JOptionPane.showMessageDialog(null, "Can't use ship any more!", "Usage Limitation", JOptionPane.ERROR_MESSAGE);
                }
            }
            if (e.getActionCommand().equals("CRUISER")) {
                if (numberOfCruiser > 0) {
                    removeActionsForKeys("just keys of selecting ship");
                    selectedItem.delete(0, selectedItem.length());
                    selectedItem.append("cruiser");
                    panelOfGameForSettingShips.setShipsOnMap(fp);
                    addActionHandler("It is only for ships");
                } else {
                    JOptionPane.showMessageDialog(null, "Can't use cruiser any more!", "Usage Limitation", JOptionPane.ERROR_MESSAGE);
                }
            }
            if (e.getActionCommand().equals("SUBMARINE")) {
                if (numberOfSubmarine > 0) {
                    removeActionsForKeys("just keys of selecting ship");
                    selectedItem.delete(0, selectedItem.length());
                    selectedItem.append("submarine");
                    panelOfGameForSettingShips.setShipsOnMap(fp);
                    addActionHandler("It is only for ships");
                } else {
                    JOptionPane.showMessageDialog(null, "Can't use submarine any more!", "Usage Limitation", JOptionPane.ERROR_MESSAGE);
                }
            }

        }
    }

    private void setActionForRotation() {
        abstractAction = new AbstractAction() {
            {
                putValue(Action.ACTION_COMMAND_KEY, getValue(Action.NAME));
            }

            public void actionPerformed(ActionEvent e) {
                rotateShipFromLastDirection();
            }
        };
        rotationButton.setAction(abstractAction);
        KeyStroke ks = KeyStroke.getKeyStroke(KeyEvent.VK_R, InputEvent.CTRL_DOWN_MASK);
        rotationButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(ks, "PRESS");
        rotationButton.getActionMap().put("PRESS", rotationButton.getAction());

        actionForResetButton = e -> {
            int position = panelOfGameForSettingShips.getShips().size() - 1;
            Ship ship = panelOfGameForSettingShips.getShips().get(position);
            panelOfGameForSettingShips.getShips().remove(position);
            setNumberOfShipsInMap();
            deleteShipOnMap(ship.getLength(), ship.getDirection(), ship.getX(), ship.getY());
        };
        resetButton.addActionListener(actionForResetButton);
    }

    private void rotateShipFromLastDirection() {
        int position = panelOfGameForSettingShips.getShips().size() - 1;
        Ship ship = panelOfGameForSettingShips.getShips().get(position);
        panelOfGameForSettingShips.getShips().remove(position);
        deleteShipOnMap(ship.getLength(), ship.getDirection(), ship.getX(), ship.getY());
        setDirectionForShip(ship);
        while (!panelOfGameForSettingShips.setShipsOnMap(ship)) {
            setDirectionForShip(ship);
        }
        panelOfGameForSettingShips.setDirection();
    }

    private void setDirectionForShip(Ship ship) {
        switch (ship.getDirection()) {
            case "right":
                ship.setDirection("down");
                break;
            case "down":
                ship.setDirection("left");
                break;
            case "left":
                ship.setDirection("up");
                break;
            case "up":
                ship.setDirection("right");
                break;
        }
    }

    private void deleteShipOnMap(int length, String direction, int x, int y) {
        switch (direction) {
            case "right":
                deleteShipFromRightDirection(length, x, y);
                break;
            case "down":
                deleteShipFromDownDirection(length, x, y);
                break;
            case "left":
                deleteShipFromLeftDirection(length, x, y);
                break;
            case "up":
                deleteShipFromUpDirection(length, x, y);
                break;
        }
    }

    private void deleteShipFromRightDirection(int length, int x, int y) {
        for (int i = y; i < y + length; i++) {
            checkRightOrLeftDirection(x, i);
        }
        for (int i = x - 1; i <= x + 1; i++) {
            try {
                panelOfGameForSettingShips.getCells()[i][y - 1].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                panelOfGameForSettingShips.getCells()[i][y + length].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
    }

    private void deleteShipFromLeftDirection(int length, int x, int y) {
        for (int i = y; i > y - length; i--) {
            checkRightOrLeftDirection(x, i);
        }
        for (int i = x - 1; i <= x + 1; i++) {
            try {
                panelOfGameForSettingShips.getCells()[i][y + 1].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                panelOfGameForSettingShips.getCells()[i][y - length].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
    }


    private void checkRightOrLeftDirection(int x, int i) {
        panelOfGameForSettingShips.getCells()[x][i].setIcon(panelOfGameForSettingShips.getEmptyCell());
        panelOfGameForSettingShips.getCells()[x][i].setIsEmpty(true);
        panelOfGameForSettingShips.getCells()[x][i].setIsBorderOfShip(false);
        try {
            panelOfGameForSettingShips.getCells()[x + 1][i].setIsBorderOfShip(false);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            panelOfGameForSettingShips.getCells()[x - 1][i].setIsBorderOfShip(false);
        } catch (ArrayIndexOutOfBoundsException ignored) {

        }
    }

    private void deleteShipFromDownDirection(int length, int x, int y) {
        for (int i = x; i < x + length; i++) {
            checkDownOrUpDirection(y, panelOfGameForSettingShips.getCells()[i]);
        }
        for (int i = y - 1; i < y + 1; i++) {
            try {
                panelOfGameForSettingShips.getCells()[x - 1][i].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                panelOfGameForSettingShips.getCells()[x + length][i].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
    }

    private void deleteShipFromUpDirection(int length, int x, int y) {
        for (int i = x; i > x - length; i--) {
            checkDownOrUpDirection(y, panelOfGameForSettingShips.getCells()[i]);
        }
        for (int i = y - 1; i < y + 1; i++) {
            try {
                panelOfGameForSettingShips.getCells()[x + 1][i].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
            try {
                panelOfGameForSettingShips.getCells()[x - length][i].setIsBorderOfShip(false);
            } catch (ArrayIndexOutOfBoundsException ignored) {
            }
        }
    }

    private void checkDownOrUpDirection(int y, Cell[] cells) {
        cells[y].setIcon(panelOfGameForSettingShips.getEmptyCell());
        cells[y].setIsEmpty(true);
        cells[y].setIsBorderOfShip(false);
        try {
            cells[y - 1].setIsBorderOfShip(false);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
        try {
            cells[y + 1].setIsBorderOfShip(false);
        } catch (ArrayIndexOutOfBoundsException ignored) {
        }
    }

    void setNumberOfShipsInMap() {
        numberOfNavy = 1;
        numberOfShip = 1;
        numberOfCruiser = 1;
        numberOfSubmarine = 1;

        for (Ship s : panelOfGameForSettingShips.getShips()) {
            switch (s.getKind()) {
                case "navy":
                    numberOfNavy--;
                    break;
                case "ship":
                    numberOfShip--;
                    break;
                case "cruiser":
                    numberOfCruiser--;
                    break;
                case "submarine":
                    numberOfSubmarine--;
                    break;
            }
        }
    }


    void setSelectedItem(StringBuffer selectedItem) {
        this.selectedItem = selectedItem;
    }

    StringBuffer getSelectedItem() {
        return selectedItem;
    }
}
