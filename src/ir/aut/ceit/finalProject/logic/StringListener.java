package ir.aut.ceit.finalProject.logic;

public interface StringListener {
   void performString(String message);

}
