package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

public class StringMessage extends BaseMessage {

    private String mStringMessage;

    public StringMessage(String s) {
        mStringMessage = s;
        serialize();
    }

    public StringMessage(byte[] serialized) {
        this.mSerialized = serialized;
        deserialize();
    }

    @Override
    public void serialize() {
        int stringLength = mStringMessage.getBytes().length;
        int messageLength = 4 + 1 + 1 + 4 + stringLength;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.STRING_MESSAGE);
        byteBuffer.putInt(stringLength);
        byteBuffer.put(mStringMessage.getBytes());
        mSerialized = byteBuffer.array();


    }

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
        int stringLength = byteBuffer.getInt();
        byte[] stringBytes = new byte[stringLength];
        byteBuffer.get(stringBytes);
        mStringMessage = new String(stringBytes);

    }


    public void test(String s) {
        mStringMessage = s;
        System.out.println(mStringMessage);
        serialize();
        deserialize();
        System.out.println(mStringMessage);
    }

    public String getStringMessage() {
        return mStringMessage;
    }

    @Override
    public byte getMessageType() {
        return MessageTypes.STRING_MESSAGE;
    }
}
