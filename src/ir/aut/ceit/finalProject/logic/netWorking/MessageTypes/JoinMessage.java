package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

public class JoinMessage extends BaseMessage {

    public JoinMessage(byte[] data) {
        mSerialized = data;
        deserialize();
    }

    public String getName() {
        return mName;
    }

    public String getIP() {
        return mIP;
    }

    public JoinMessage(String name, String yourIP) {
        this.mName = name;
        mIP = yourIP;

    }

    private String mName;
    private String mIP;

    @Override
    public void serialize() {
        int messageLength = 4 + 1 + 1 + 4 + mName.getBytes().length + 4 + mIP.getBytes().length;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.JOIN_MESSAGE);
        int nameLength = mName.getBytes().length;
        byteBuffer.putInt(nameLength);
        byteBuffer.put(mName.getBytes());
        int IPLength = mIP.getBytes().length;
        byteBuffer.putInt(IPLength);
        byteBuffer.put(mIP.getBytes());
        mSerialized = byteBuffer.array();

    }

    byte messageType;

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        messageType = byteBuffer.get();
        int nameLength = byteBuffer.getInt();
        System.out.println("name lenght" + nameLength);
        byte[] nameBytes = new byte[nameLength];
        byteBuffer.get(nameBytes);
        mName = new String(nameBytes);
        int IPLength = byteBuffer.getInt();
        byte[] IPBytes = new byte[IPLength];
        byteBuffer.get(IPBytes);
        mIP = new String(IPBytes);
    }

    @Override
    public byte getMessageType() {
        return messageType;
    }
}
