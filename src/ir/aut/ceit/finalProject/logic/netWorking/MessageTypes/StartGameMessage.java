package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

/**
 * Created by ASUS on 06/07/2017.
 */
public class StartGameMessage extends BaseMessage {


    public StartGameMessage(byte[] data) {
        mSerialized = data;
        deserialize();
    }

    public StartGameMessage() {
        serialize();
    }

    @Override
    public void serialize() {
        byte[] nameBytes = Main.getName().getBytes();
        int messageLength = 4 + 1 + 1 + 4 + nameBytes.length;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.START_GAME_MESSAGE);
        byteBuffer.putInt(nameBytes.length);
        byteBuffer.put(nameBytes);
        mSerialized = byteBuffer.array();


    }

    byte messageType;

    public String getName() {
        return name;
    }

    private String name;

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        messageType = byteBuffer.get();
        int nameLen = byteBuffer.getInt();
        byte[] nameBytes = new byte[nameLen];
        byteBuffer.get(nameBytes);
        name = new String(nameBytes);

    }

    @Override
    public byte getMessageType() {
        return messageType;
    }


}
