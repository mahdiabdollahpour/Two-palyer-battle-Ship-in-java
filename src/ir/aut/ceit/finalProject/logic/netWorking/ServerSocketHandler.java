package ir.aut.ceit.finalProject.logic.netWorking;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;

public class ServerSocketHandler extends Thread {
    private ServerSocket mServerSocket;
    private INetworkHandlerCallback mNetworkHandlerCallback;
    private IServerSocketHandlerCallback mServerSocketHandlerCallback;

    public ServerSocketHandler(int port, INetworkHandlerCallback iNetworkHandlerCallback,
                               IServerSocketHandlerCallback iServerSocketHandlerCallback) throws IOException {

        mServerSocket = new ServerSocket(port);
        mNetworkHandlerCallback = iNetworkHandlerCallback;
        mServerSocketHandlerCallback = iServerSocketHandlerCallback;

    }

    /**
     * While server socket is connected and stop is not called:
     * if a connection receives, then create a network handler and pass it through
     * onNewConnectionReceived  else sleep for 100 ms.
     */
    @Override
    public void run() {

        while (!this.isInterrupted()) {
            Socket s = null;
            try {
                System.out.println("in the server socket handler loop in the THREAD");
                s = mServerSocket.accept();
                System.out.println("accepted a socket");
                mServerSocketHandlerCallback.onNewConnectionReceived(new NetworkHandler(s, mNetworkHandlerCallback));
                Thread.sleep(100);
            } catch (SocketException e) {
                //socket has been closed
            } catch (IOException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }

    /**
     * Kill the thread and close the server socket.
     */
    public void stopSelf() throws IOException {
        this.interrupt();
        mServerSocket.close();
    }

}