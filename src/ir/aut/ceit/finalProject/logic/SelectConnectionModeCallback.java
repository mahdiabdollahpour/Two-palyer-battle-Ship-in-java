package ir.aut.ceit.finalProject.logic;

public interface SelectConnectionModeCallback {
    void connectAsHost(String port,String name);
    void connectAsGuest(String IP,String port,String name);
}
