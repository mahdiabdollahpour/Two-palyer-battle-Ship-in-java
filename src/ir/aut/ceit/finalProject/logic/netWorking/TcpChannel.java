package ir.aut.ceit.finalProject.logic.netWorking;


import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;
import java.nio.ByteBuffer;

public class TcpChannel {
    private Socket mSocket;
    private OutputStream mOutputStream;
    private InputStream mInputStream;

    public TcpChannel(SocketAddress socketAddress, int timeout) throws IOException {
        mSocket = new Socket();
        mSocket.connect(socketAddress);
        mOutputStream = mSocket.getOutputStream();
        mInputStream = mSocket.getInputStream();
    }

    public TcpChannel(Socket socket, int timeout) throws IOException {
        this.mSocket = socket;
        mOutputStream = mSocket.getOutputStream();
        mInputStream = mSocket.getInputStream();
    }

    /**
     * Try to read specific count from input stream.
     */
    public byte[] read(final int count) throws IOException {
        //   for (int i = 0; i < count; i++) {
        int available = mInputStream.available();

        if (available > 0) {
            System.out.println("available  " + available);
            int length = 0;
            for (int i = 0; i < 4; i++) {
                int a = mInputStream.read();
                length *= 64;
                length += a;
            }
            System.out.println("available  confirmation" + mInputStream.available());
            System.out.println("lenght " + length);
            byte[] data = new byte[length - 4];
            mInputStream.read(data);
            ByteBuffer bb = ByteBuffer.allocate(length);
            bb.putInt(length);
            bb.put(data);
            return bb.array();
        }

        return null;
//        }
    }

    /**
     * Write bytes on output stream.
     */
    public void write(byte[] data) throws IOException {
        mOutputStream.write(data, 0, data.length);
        System.out.println("data was written in outputStream in tcpchannel");
    }

    /**
     * Check socket’s connectivity.
     */
    public boolean isConnected() {
        return mSocket.isConnected();
    }

    /**
     * Try to close socket and input-output streams.
     */
    public void closeChannel() throws IOException {

        mSocket.close();
        mInputStream.close();
        mOutputStream.close();
    }
}