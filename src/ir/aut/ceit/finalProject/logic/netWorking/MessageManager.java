package ir.aut.ceit.finalProject.logic.netWorking;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.logic.*;
import ir.aut.ceit.finalProject.logic.netWorking.MessageTypes.*;
import ir.aut.ceit.finalProject.view.ConnectionRequest;
import ir.aut.ceit.finalProject.view.WaitForConnection;
import ir.aut.ceit.finalProject.view.WaitForHostToJoin;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class MessageManager implements IServerSocketHandlerCallback, INetworkHandlerCallback,
        ActionListener, AttackingStateCallBack, ReadyAndUnreadyCallback,CancelCallBack,SelectTurnCallBack {
    private List<NetworkHandler> mNetworkHandlerList;
    private ServerSocketHandler mServerSocketHandler;
    private StringListener mChatBoxCallback;
    private static int mIndex = 0;
    private static boolean mOpponentReady;

    private static boolean mAreYouReady;
    private static boolean mYourTurn;
    private static boolean mOpponentTurn;

    private boolean mYouDidCancel;
    public void setChatBoxCallback(StringListener chatBoxCallback) {
        mChatBoxCallback = chatBoxCallback;
    }

    public StringListener getCallBackFromMessageManagerToChatBox() {
        return mCallBackFromMessageManagerToChatBox;
    }

    private StringListener mCallBackFromMessageManagerToChatBox = new StringListener() {
        @Override
        public void performString(String message) {
            sendStringMessage(message);
        }
    };

    /**
     * Instantiate server socket handler and start it. (Call this constructor in host mode)
     */
    public MessageManager(int port) {
        mNetworkHandlerList = new ArrayList<>();
        try {
            mServerSocketHandler = new ServerSocketHandler(port, this, this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mServerSocketHandler.start();
        mOpponentReady = false;
        mOpponentTurn = false;
        mYouDidCancel = false;
        mYourTurn = false;
        mAreYouReady = false;
    }

    /**
     * Instantiate a network handler and start it. (Call this constructor in guest mode)
     */
    public MessageManager(String ip, int port) {
        mNetworkHandlerList = new ArrayList<>();
        NetworkHandler networkHandler = null;
        try {
            networkHandler = new NetworkHandler(new Socket(ip, port), this);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mNetworkHandlerList.add(networkHandler);
        networkHandler.start();
        mOpponentReady = false;
        mOpponentTurn = false;
        mYouDidCancel = false;
        mYourTurn = false;
        mAreYouReady = false;
    }

    /**
     * IMPORTANT: Request login is an example message and doesn’t relate to this project! *
     * Create a RequestLoginMessage object and sent it through the appropriate network handler.
     */
    public void sendRequestLogin(String to, String username, String password) {
    }

    /**
     * IMPORTANT: Request login is an example message and doesn’t relate to this project! * Use the message.
     */
    private void consumeRequestLogin(RequestLoginMessage message) {


    }

    private void consumeStringMessage(StringMessage message) {
        mChatBoxCallback.performString(message.getStringMessage());
    }

    public void sendStringMessage(String message) {
        StringMessage sm = new StringMessage(message);
        mNetworkHandlerList.get(mIndex).sendMessage(sm);
    }


    public void sendAttackMessage(int x, int y) {
        if (mYourTurn) {
            AttackMessage attackMessage = new AttackMessage(x, y);
            mNetworkHandlerList.get(mIndex).sendMessage(attackMessage);
        }
    }

    private void consumeAttackMessage(AttackMessage message) {
        if (Main.getCells().getCells()[message.getX()][message.getY()].isEmpty()) {
            sendAttackFeedbackMessage(message.getX(), message.getY(), (byte) 0);
            Main.emptyCellOfYou(message.getX(), message.getY());
            setTurn(1, 0);
        } else {
            sendAttackFeedbackMessage(message.getX(), message.getY(), (byte) 1);
            Main.shutCellOfYou(message.getX(), message.getY());
        }
    }

    private void sendAttackFeedbackMessage(int x, int y, byte feedback) {
        AttackFeedbackMessage attackFeedbackMessage = new AttackFeedbackMessage(x, y, feedback);
        mNetworkHandlerList.get(mIndex).sendMessage(attackFeedbackMessage);
    }

    private void consumeAttackFeedbackMessage(AttackFeedbackMessage message) {
        if (message.getFeedback() == 1) {
            Main.shutCellOfOpponent(message.getX(), message.getY());
        } else {
            Main.emptyCellOfOpponent(message.getX(), message.getY());
            sendTurnMessage(0, 1);
        }
    }

    private void consumeLeaveMessage(LeaveMessage message) {
        System.out.println("consuming leave message");
        Main.getBaseFrame().dispose();
        Main.runSelectConnection();
        JOptionPane.showMessageDialog(null, "Opponent has left the game.", "Notification", JOptionPane.INFORMATION_MESSAGE);
        onSocketClosed();
    }

    public void sendLeaveMessage() {
        mNetworkHandlerList.get(mIndex).sendMessage(new LeaveMessage());
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        onSocketClosed();

    }

   public void sendStartGameMessage() {
        mNetworkHandlerList.get(mIndex).sendMessage(new StartGameMessage());
    }

    private void consumeStartGameMessage(StartGameMessage startGameMessage) {
        WaitForHostToJoin.getInstance().setVisible(false);
        Main.setOpponentName(startGameMessage.getName());
        Main.startGame();
    }


    private void consumeJoinMessage(JoinMessage message) {
        ConnectionRequest connectionRequest = new ConnectionRequest(message.getName(), message.getIP());
        connectionRequest.setRequestConnectionListener(new RequestConnectionCallback() {
            @Override
            public void acceotPerformed(int integer) {
                mIndex = integer;
                Main.setOpponentIP(message.getIP());
                Main.setOpponentName(message.getName());
                Main.startGame();
                sendStartGameMessage();
            }

            @Override
            public void rejectPerformed(int destination) {
                mNetworkHandlerList.get(destination).sendMessage(new RejectMessage());
            }
        });

        WaitForConnection.makeRequest(connectionRequest);
    }

    public void sendJoinMessage(String name, String IP) {
        JoinMessage joinMessage = new JoinMessage(name, IP);
        joinMessage.serialize();
        mNetworkHandlerList.get(mIndex).sendMessage(joinMessage);
    }


    public static boolean ismYourTurn() {
        return mYourTurn;
    }

    private void consumeReadyMessage() {
        mOpponentReady = true;
        JOptionPane.showMessageDialog(Main.getBaseFrame(), Main.getOpponentName() + " is ready!");
        if (mAreYouReady) {
            JOptionPane.showMessageDialog(Main.getBaseFrame(), "Now let's play!!!");
            Main.removeFooterForChoosingShip();
            if(mYourTurn) {
                Main.replaceCellsWithCellsOfApponent();
            }
        }
    }

    public void sendReadyMessage() {
        ReadyMessage readyMessage = new ReadyMessage();
        mAreYouReady = true;
        mNetworkHandlerList.get(mIndex).sendMessage(readyMessage);
    }

    @Override
    public void sendUnreadyMessage() {
        UnreadyMessage unreadyMessage = new UnreadyMessage();
        mAreYouReady = false;
        mNetworkHandlerList.get(mIndex).sendMessage(unreadyMessage);
    }

    private void consumeUnreadyMessage() {
        mOpponentReady = false;
        JOptionPane.showMessageDialog(Main.getBaseFrame(), Main.getName() + " has canceled ready.");
    }

    @Override
    public void sendTurnMessage(int yours, int opponents) {
        TurnMessage turnMessage = new TurnMessage(yours, opponents);
        System.out.println("turn message sent");
        System.out.println(yours + " , " + opponents);
        setTurn(yours, opponents);
        mNetworkHandlerList.get(mIndex).sendMessage(turnMessage);
    }

    private void consumeTurnMessage(TurnMessage turnMessage) {
        System.out.println("turn message consumed");
        setTurn(turnMessage.getYourTurn(), turnMessage.getOpponentTurn());
    }

    public void sendVictoryMessage() {
        VictoryMessage victoryMessage = new VictoryMessage();
        mNetworkHandlerList.get(mIndex).sendMessage(victoryMessage);
    }

    private void consumeVictoryMessage(VictoryMessage victoryMessage) {
        JLabel label = new JLabel();
        label.setIcon(new ImageIcon("victory.png"));
        JOptionPane.showMessageDialog(Main.getBaseFrame(), label, "VICTORY", JOptionPane.PLAIN_MESSAGE);
    }

    private void setTurn(int yours, int opponents) {
        if (yours == 1) {
            mYourTurn = true;
            mOpponentTurn = false;
        } else if (opponents == 1) {
            mOpponentTurn = true;
            mYourTurn = false;
        } else if (yours == 0 && opponents == 0) {
            mYourTurn = false;
            mOpponentTurn = false;
        }
    }

    /**
     * Add network handler to the list.
     */
    @Override
    public void onNewConnectionReceived(NetworkHandler networkHandler) {
        networkHandler.start();
        mNetworkHandlerList.add(networkHandler);
    }

    /**
     * IMPORTANT: Request login is an example message and doesn’t relate to this project!
     * According to the message type of baseMessage, call corresponding method to use it.
     */
    @Override
    public void onMessageReceived(BaseMessage baseMessage) {
        baseMessage.deserialize();
        System.out.println(baseMessage.getMessageType());
        switch (baseMessage.getMessageType()) {
            case MessageTypes.REQUEST_LOGIN:
                consumeRequestLogin((RequestLoginMessage) baseMessage);
                break;
            case MessageTypes.STRING_MESSAGE:
                consumeStringMessage((StringMessage) baseMessage);
                break;
            case MessageTypes.ATTACK_MESSAGE:
                consumeAttackMessage((AttackMessage) baseMessage);
                break;
            case MessageTypes.ATTACK_FEEDBACK_MESSAGE:
                consumeAttackFeedbackMessage((AttackFeedbackMessage) baseMessage);
                break;
            case MessageTypes.LEAVE_MESSAGE:
                consumeLeaveMessage((LeaveMessage) baseMessage);
                break;
            case MessageTypes.JOIN_MESSAGE:
                consumeJoinMessage((JoinMessage) baseMessage);
                break;
            case MessageTypes.READY_MESSAGE:
                consumeReadyMessage();
                break;
            case MessageTypes.START_GAME_MESSAGE:
                consumeStartGameMessage((StartGameMessage) baseMessage);
                break;
            case MessageTypes.UNREADY_MESSAGE:
                consumeUnreadyMessage();
                break;
            case MessageTypes.TURN_MESSAGE:
                consumeTurnMessage((TurnMessage) baseMessage);
                break;
            case MessageTypes.REJECT_MESSAGE:
                consumeRejectMessage((RejectMessage) baseMessage);
                break;
            case MessageTypes.VICTORY_MESSAGE:
                consumeVictoryMessage((VictoryMessage) baseMessage);
                break;
            case MessageTypes.CANCEL_MESSAGE:
                consumeCancelMessage((CancelMessage) baseMessage);
                break;
            default:
                break;

        }
    }

    public void sendCancelMessage() {
        mNetworkHandlerList.get(mIndex).sendMessage(new CancelMessage());
        mYouDidCancel = true;
        System.out.println("you Did cancel (true)");
    }

    private void consumeCancelMessage(CancelMessage cancelMessage) {
        WaitForConnection.cancelRequest(cancelMessage.getIp());
    }

    private void consumeRejectMessage(RejectMessage baseMessage) {
        WaitForHostToJoin.getInstance().disPlayHostRejected(mYouDidCancel);
        WaitForHostToJoin.getInstance().setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        Main.runSelectConnection();
    }

    public void sendRejectMessage() {
        mNetworkHandlerList.get(mIndex).sendMessage(new RejectMessage());
    }

    public void setIndex(int Index) {
        mIndex = Index;
    }

    @Override
    public void onSocketClosed() {
        if (mServerSocketHandler != null) {
            try {
                mServerSocketHandler.stopSelf();
                mNetworkHandlerList.get(mIndex).stopSelf();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        mIndex = 0;
        ConnectionRequest.setIndexingToZero();
        mNetworkHandlerList = null;
    }

    public static boolean isOpponentReady() {
        return mOpponentReady;
    }

    @Override
    public void actionPerformed(ActionEvent e) {//for leave action
        Main.getBaseFrame().dispose();
        sendLeaveMessage();
        Main.runSelectConnection();
    }

}