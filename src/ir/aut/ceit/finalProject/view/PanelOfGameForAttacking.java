package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.logic.AttackingStateCallBack;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class PanelOfGameForAttacking extends JPanel {

    private Cell[][] cells = new Cell[10][10];
    private ImageIcon ship = new ImageIcon("ship.PNG");
    private ImageIcon emptyCell = new ImageIcon("emptyCell.PNG");
 //   private FooterForChoosingShip fp;
    private ArrayList<Ship> ships = new ArrayList<>();
    private String direction = "noDirection";
    private AttackingStateCallBack mAttackingStateCallBack;

    public PanelOfGameForAttacking(AttackingStateCallBack attackingStateCallBack) {
        setLayout(new GridBagLayout());
        this.setPreferredSize(new Dimension(10 * Cell.mDimension.width, 10 * Cell.mDimension.height));
        mAttackingStateCallBack = attackingStateCallBack;
        GridBagConstraints gbc = new GridBagConstraints();
        for (int i = 0; i < 10; i++)
            for (int j = 0; j < 10; j++) {
                if (j == 0) {
                    gbc.gridy = j;
                    gbc.gridx = i + 1;
                    JLabel label = new JLabel((i + 1) + "");
                    add(label, gbc);
                }
                if (i == 0) {
                    gbc.gridy = j + 1;
                    gbc.gridx = i;
                    JLabel label = new JLabel((char) (j + 65) + "");
                    add(label, gbc);
                }
                gbc.gridy = j + 1;
                gbc.gridx = i + 1;
                cells[i][j] = new Cell(emptyCell);
                add(cells[i][j], gbc);
                cells[i][j].addMouseListener(new SetEventHandler(i, j));
            }

    }

    private class SetEventHandler extends MouseAdapter {

        int xPosition;
        int yPosition;

        SetEventHandler(int x, int y) {
            xPosition = x;
            yPosition = y;
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            super.mouseClicked(e);
            System.out.println("coords are: " + xPosition + "," + yPosition);
            mAttackingStateCallBack.sendAttackMessage(xPosition, yPosition);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e1) {
                e1.printStackTrace();
            }
        }
    }

    public Cell[][] getCells() {
        return cells;
    }
}
