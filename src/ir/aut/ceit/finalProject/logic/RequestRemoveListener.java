package ir.aut.ceit.finalProject.logic;

import ir.aut.ceit.finalProject.view.ConnectionRequest;

import javax.swing.*;


public interface RequestRemoveListener {
    void remove(ConnectionRequest connectionRequest);
}
