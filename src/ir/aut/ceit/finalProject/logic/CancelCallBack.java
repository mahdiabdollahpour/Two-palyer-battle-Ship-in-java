package ir.aut.ceit.finalProject.logic;

/**
 * Created by ASUS on 09/07/2017.
 */
public interface CancelCallBack {
    void sendCancelMessage();
}
