package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

public class TurnMessage extends BaseMessage {

    private int yourTurn;
    private int opponentTurn;

    public TurnMessage(int yourTurn, int opponentTurn) {
        this.yourTurn = yourTurn;
        this.opponentTurn = opponentTurn;
        serialize();
    }

    public TurnMessage(byte[] data) {
        mSerialized = data;
        deserialize();
    }

    @Override
    protected void serialize() {
        int messageLength = 4 + 1 + 1 + 4 + 4;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.TURN_MESSAGE);
        byteBuffer.putInt(yourTurn);
        byteBuffer.putInt(opponentTurn);
        mSerialized = byteBuffer.array();
    }

    private byte messageType;

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        messageType = byteBuffer.get();
        opponentTurn = byteBuffer.getInt();
        yourTurn = byteBuffer.getInt();
    }

    @Override
    public byte getMessageType() {
        return messageType;
    }

    public int getYourTurn() {
        return yourTurn;
    }

    public int getOpponentTurn() {
        return opponentTurn;
    }
}
