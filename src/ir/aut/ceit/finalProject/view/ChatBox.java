package ir.aut.ceit.finalProject.view;

import ir.aut.ceit.finalProject.Main;
import ir.aut.ceit.finalProject.logic.Conversation;
import ir.aut.ceit.finalProject.logic.StringListener;
import ir.aut.ceit.finalProject.model.ReadJson;
import ir.aut.ceit.finalProject.model.SaveToFile;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;


public class ChatBox extends JPanel implements Runnable, StringListener {

    private Conversation conversation;
    private Queue<JSONObject> toBeUpdated;
    private JTextArea readChat = new JTextArea();
    private JTextField writeChat = new JTextField();
    private StringListener mCallBackFromMessageManagerToChatBox;
    private Thread toFileHistoryAdderThread;

//    private StringListener chatBoxCallback = new StringListener() {
//        @Override
//        public void performString(String message, boolean fromOpponent) {
//
//
//        }
//
//    };

    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
    private Date date;

    public ChatBox(StringListener callBackFromMessageManagerToChatBox) {
        date = new Date();
        toBeUpdated = new LinkedList();
        conversation = new Conversation(sdf.format(date));
        try {
            SaveToFile.saveJson(conversation.getToJson(), false);
        } catch (IOException e) {
            e.printStackTrace();
        }
        mCallBackFromMessageManagerToChatBox = callBackFromMessageManagerToChatBox;
        //  mMessageManager = messageManager;
        conversation.setContactIP(Main.getOpponentIP());
        conversation.setContactName(Main.getOpponentName());
        Main.setStringListener(this);
        readChat.setEditable(false);
        writeChat.setMaximumSize(new Dimension(300, 50));
        writeChat.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    JSONObject jsonObject = new JSONObject();
                    try {
                        jsonObject.put("message", writeChat.getText());

                        date = new Date();
                        jsonObject.put("date", sdf.format(date));
                        jsonObject.put("sender", Main.getName());
                    } catch (JSONException e1) {
                        e1.printStackTrace();
                    }
                    performString(jsonObject.toString());
                    mCallBackFromMessageManagerToChatBox.performString(jsonObject.toString());
                    writeChat.setText("");
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        setLayout(new BoxLayout(this, 1));
        readChat.setPreferredSize(new Dimension(100, 1000));
        writeChat.setPreferredSize(new Dimension(100, 30));
        //  setSize(new Dimension(200, 1030));
        setMinimumSize(new Dimension(500, 1030));
        add(writeChat);
        add(new JScrollPane(readChat));
        toFileHistoryAdderThread = new Thread(this);
        toFileHistoryAdderThread.start();
    }


    @Override
    public Dimension getPreferredSize() {
        return new Dimension(300, 1300);
    }

    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            if (!toBeUpdated.isEmpty()) {
                JSONObject messageJsoned = toBeUpdated.poll();
                JSONArray jsonArray = null;
                JSONObject conversationInJson = null;
                try {
                    conversationInJson = ReadJson.readHistoryWithID(conversation.getID());
                    jsonArray = conversationInJson.getJSONArray("chat");
                    jsonArray.put(jsonArray.length(), messageJsoned);
                    conversationInJson.put("chat", jsonArray);
                    SaveToFile.saveJson(conversationInJson, true);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {

                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }

    @Override
    public void performString(String message) {

        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(message);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            readChat.append(jsonObject.getString("sender") + " : " +
                    jsonObject.getString("message") + "\n" + jsonObject.getString("date") + "\n");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        conversation.addMessageToList(jsonObject);
        toBeUpdated.add(jsonObject);
    }
}
