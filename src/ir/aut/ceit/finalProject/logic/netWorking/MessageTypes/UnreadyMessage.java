package ir.aut.ceit.finalProject.logic.netWorking.MessageTypes;

import ir.aut.ceit.finalProject.logic.netWorking.BaseMessage;

import java.nio.ByteBuffer;

public class UnreadyMessage extends BaseMessage {

    public UnreadyMessage(byte[] data) {
        mSerialized = data;
        deserialize();
    }

    public UnreadyMessage() {
        serialize();
    }

    @Override
    public void serialize() {
        int messageLength = 4 + 1 + 1;
        ByteBuffer byteBuffer = ByteBuffer.allocate(messageLength);
        byteBuffer.putInt(messageLength);
        byteBuffer.put(MessageTypes.PROTOCOL_VERSION);
        byteBuffer.put(MessageTypes.UNREADY_MESSAGE);
        mSerialized = byteBuffer.array();
    }

    @Override
    public void deserialize() {
        ByteBuffer byteBuffer = ByteBuffer.wrap(mSerialized);
        int messageLength = byteBuffer.getInt();
        byte protocolVersion = byteBuffer.get();
        byte messageType = byteBuffer.get();
    }

    @Override
    public byte getMessageType() {
        return MessageTypes.UNREADY_MESSAGE;
    }


}
